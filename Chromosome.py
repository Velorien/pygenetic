import random


class Chromosome:
    alleles = []

    def __init__(self, alleles):
        self.alleles = alleles

    def mutate(self, possiblevalues, chance=0.01, allowmultiple=False):
        """Creates chromosome with default chance of mutation of 1% (0.01)"""
        random.seed()
        mutated = Chromosome([a for a in self.alleles])
        if allowmultiple:
            for i in range(len(mutated.alleles)):
                if random.random() < chance:
                    mutated.alleles[i] = possiblevalues[random.randint(0, len(possiblevalues) - 1)]
        else:
            if random.random() < chance:
                index = random.randint(0, len(mutated.alleles) - 1)
                mutated.alleles[index] = possiblevalues[random.randint(0, len(possiblevalues) - 1)]

        return mutated

    def mutate(self, function=lambda x: x, chance=0.01):
        """Creates chromosome with default chance of mutation of 1% and custom mutation function"""
        random.seed()
        if random.random() < chance:
            return function(self)
        else:
            return self

    def cross(self, other, point=0):
        """Combines two chromosomes at given point"""
        if point == 0:
            random.seed()
            point = random.randint(1, len(self.alleles) - 1)

        return Chromosome(self.alleles[:point] + other.alleles[point:])